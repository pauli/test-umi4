# Ant Design Pro 4 / UMI 学习

1. [Ant Design Pro](https://pro.ant.design), [github](https://github.com/ant-design/ant-design-pro).
2. [ProComponents](https://procomponents.ant.design/)
3. [UmiJS](https://umijs.org/)

## Development

```bash
npm install
yarn
npm start
npm run build
npm run lint --fix
npm run lint:fix
npm test
```
