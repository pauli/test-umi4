import G6 from '@antv/g6';

/**
 * 脑图树 https://antv-g6.gitee.io/zh/examples/tree/mindmap#hRightMindmap
 */
export function showGraph(container: HTMLDivElement | null, data: any) {
  if (!container || !data) {
    return;
  }
  const width = container.scrollWidth;
  const height = container.scrollHeight || 500;
  const graph = new G6.TreeGraph({
    container,
    width,
    height,
    modes: {
      default: [
        {
          type: 'collapse-expand',
          onChange: function onChange(item, collapsed) {
            const model = item?.get('model') || {};
            model.collapsed = collapsed;
            return true;
          },
        },
        'drag-canvas',
        'zoom-canvas',
      ],
    },
    defaultNode: {
      size: 26,
      anchorPoints: [
        [0, 0.5],
        [1, 0.5],
      ],
    },
    defaultEdge: {
      type: 'cubic-horizontal',
    },
    layout: {
      type: 'mindmap',
      direction: 'H',
      getHeight: () => {
        return 16;
      },
      getWidth: () => {
        return 16;
      },
      getVGap: () => {
        return 10;
      },
      getHGap: () => {
        return 100;
      },
      getSide: () => {
        return 'right';
      },
    },
  });

  let centerX = 0;
  graph.node((node) => {
    if (node.id === 'Modeling Methods') {
      centerX = node.x || 0;
    }

    return {
      label: node.id,
      labelCfg: {
        // eslint-disable-next-line no-nested-ternary
        position: node.children?.length ? 'right' : (node.x || 0) > centerX ? 'right' : 'left',
        offset: 5,
      },
    };
  });

  graph.data(data);
  graph.render();
  graph.fitView();

  if (typeof window !== 'undefined')
    window.onresize = () => {
      if (!graph || graph.get('destroyed')) return;
      if (!container || !container.scrollWidth || !container.scrollHeight) return;
      graph.changeSize(container.scrollWidth, container.scrollHeight);
    };
}
