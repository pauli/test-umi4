import { useRef } from "react";
import { showGraph } from "./graph";

export default () => {

  const treeGraphDiv = useRef(null);

  fetch('https://gw.alipayobjects.com/os/antvdemo/assets/data/algorithm-category.json')
    .then((res) => res.json())
    .then((data) => {
      showGraph(treeGraphDiv.current, data);
    });
    return <div style={{ height: '100vh' }} ref={treeGraphDiv}></div>;
};
